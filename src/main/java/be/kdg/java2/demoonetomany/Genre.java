package be.kdg.java2.demoonetomany;

public enum Genre {
    THRILLER, BIOGRAPHY, FANTASY
}
