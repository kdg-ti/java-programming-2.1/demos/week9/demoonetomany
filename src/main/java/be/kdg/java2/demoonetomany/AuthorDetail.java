package be.kdg.java2.demoonetomany;

import javax.persistence.*;

@Entity
@Table(name="author_details")
public class AuthorDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String email;

    @OneToOne(mappedBy = "authorDetail", cascade = CascadeType.ALL)
    private Author author;

    protected AuthorDetail() {
    }

    public AuthorDetail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public String toString() {
        return "AuthorDetail{" +
                "id=" + id +
                ", email='" + email + '\'' +
                '}';
    }
}
