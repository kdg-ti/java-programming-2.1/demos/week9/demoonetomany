package be.kdg.java2.demoonetomany;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Table(name = "books")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title", nullable = false, length = 50)
    private String title;

    @Column(name = "pages")
    private int pages;

    @Column(name = "pub_date")
    private LocalDate datePublished;

    @Column(name = "best_reading_time")
    private LocalTime bestReadingTime;

    @Enumerated(EnumType.STRING)
    private Genre genre;

    @OneToMany(mappedBy = "book", cascade = CascadeType.ALL)
    private List<Review> reviews;

    protected Book() {
    }

    public Book(String title, int pages, LocalDate datePublished, LocalTime bestReadingTime, Genre genre) {
        this.title = title;
        this.pages = pages;
        this.datePublished = datePublished;
        this.bestReadingTime = bestReadingTime;
        this.genre = genre;
    }

    public static Book randomBook() {
        Random random = new Random();
        return new Book("title" + random.nextInt(1000),
                random.nextInt(1000),
                LocalDate.of(1900 + random.nextInt(100), random.nextInt(12) + 1, random.nextInt(28) + 1),
                LocalTime.of(random.nextInt(24), random.nextInt(60)),
                Genre.values()[random.nextInt(Genre.values().length)]);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public void addReview(Review review){
        if (reviews==null) {
            reviews = new ArrayList<>();
        }
        reviews.add(review);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public LocalDate getDatePublished() {
        return datePublished;
    }

    public void setDatePublished(LocalDate datePublished) {
        this.datePublished = datePublished;
    }

    public LocalTime getBestReadingTime() {
        return bestReadingTime;
    }

    public void setBestReadingTime(LocalTime bestReadingTime) {
        this.bestReadingTime = bestReadingTime;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pages=" + pages +
                ", datePublished=" + datePublished +
                ", bestReadingTime=" + bestReadingTime +
                ", genre=" + genre +
                '}';
    }
}
