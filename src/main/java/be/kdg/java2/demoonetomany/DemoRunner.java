package be.kdg.java2.demoonetomany;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Component
public class DemoRunner implements CommandLineRunner {
    @PersistenceUnit
    private EntityManagerFactory entityManagerFactory;

    @Override
    public void run(String... args) throws Exception {
        Book book = Book.randomBook();
        Review review1 = new Review("Great book!");
        review1.setBook(book);
        Review review2 = new Review("Horrible book!");
        review2.setBook(book);
        book.addReview(review1);
        book.addReview(review2);

        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(book);
        em.getTransaction().commit();
        em.close();

        em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        book = em.find(Book.class, 1);
        System.out.println("Loading the reviews...");
        List<Review> reviews = book.getReviews();
        reviews.forEach(System.out::println);
        em.getTransaction().commit();
        em.close();

//        em = entityManagerFactory.createEntityManager();
//        em.getTransaction().begin();
//        Review review = em.find(Review.class, 1);
//        em.remove(review);
//        em.getTransaction().commit();
//        em.close();
//
//        em = entityManagerFactory.createEntityManager();
//        em.getTransaction().begin();
//        book = em.find(Book.class, 1);
//        em.remove(book);
//        em.getTransaction().commit();
//        em.close();
    }
}
