package be.kdg.java2.demoonetomany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoOneToManyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoOneToManyApplication.class, args);
	}

}
